gnit
=====

Tool for ensuring that a Git repository is set up correctly (for me). The tool is able to check (and report) if everything is set up and optionally Fix It™

... and only writing this tool in Go to learn the language. Would have been much
easier to Bash it but not this time :)

Help and usage:

    gnit help


__Things planned for this amazing tool aka. "Roadmap":__  

* Backup files before writing (seems like a _Good Idea_)
* Unit tests
* Example perhaps
* Make ZSH completion script

## Release notes

### 1.5

* Add small util func to check if path is a directory

### 1.4

* Minor clean-up to documentation
* Make downloaded Git hooks executable

### 1.3

Check and fix added for
* Check that host `gerrit` is available for SSH

### 1.2

Check and fix added for
* Check for nocommit hook to block unwanted commits
* Read flags `-all`, `-repo` and `-global`

### 1.1

Check and fix added for
* Check for Gerrit hook that inserts Change Id

### 1.0

Checks added for
* Global Git config
* Git repo config
* Check for Gerrit review remote and `refs/for/develop` 
