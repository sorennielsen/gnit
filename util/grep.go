package util

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
)

// Grep looks for match of `re` in `file` and returns true if match is found.
func Grep(re string, file string) (bool, error) {
	regex, err := regexp.Compile(re)
	if err != nil {
		panic(fmt.Sprintf("Not a valid regexp: %s", re))
	}

	fh, err := os.Open(file)
	if err != nil {
		return false, errors.New(fmt.Sprintf("\t'%s' does not exist.\n", file))
	}
	defer fh.Close()

	f := bufio.NewReader(fh)
	buf := make([]byte, 1024)
	for {
		buf, _, err = f.ReadLine()
		if err != nil {
			if err == io.EOF {
				return false, nil
			}
			return false, errors.New("Unable to read from file. " + err.Error())
		}

		if regex.MatchString(string(buf)) {
			return true, nil
		}

	}

	return false, nil
}
