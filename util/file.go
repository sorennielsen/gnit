// Small helper utility for working with files (and directories)
package util

import (
	"os"
)

// Check if file or directory exists.
// Will panic if stat(1) fails.
func FileExist(filename string) bool {
	if _, err := os.Stat(filename); err != nil {
		if os.IsNotExist(err) {
			return false
		}
		panic("Unable to stat(1) " + filename + ". " + err.Error())
	}
	return true
}

// Check if given path points to a directory.
// Will panic if stat(1) fails.
func IsDir(path string) bool {
	fi, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false
		}
		panic("Unable to stat(1) " + path + ". " + err.Error())
	}
	return fi.IsDir()
}
