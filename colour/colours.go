package colour

// Some colours
const (
	Reset      = "\033[0m"
	Boldf      = "\033[1m%v\033[0m"
	Underlinef = "\033[4m%v\033[0m"
	FgRedf     = "\033[31m%v\033[0m"
	FgGreenf   = "\033[32m%v\033[0m"
	FgBluef    = "\033[34m%v\033[0m"
	FgYellowf  = "\x1b[33m%v\033[0m"
)
