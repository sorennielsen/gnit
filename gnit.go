/*
Gnit is a program for checking if new Git repositories has been initialised properly.
Not in the sense of the `git init` command but in the sense of configuration and hooks.

What it does:
    - sanity checks on the global configuration
    - checks configuration of repository
    - checks for expected Git hooks
    - offers to fix known problems

Usage:
    gnit [help|check|fix] [options]

See `gnit help` for details.

*/
package main

import (
	"flag"
	"fmt"
	"os"
	"sync"

	"github.com/sorennielsen/gnit/colour"
	"github.com/sorennielsen/gnit/gitconfig"
	"github.com/sorennielsen/gnit/sshconfig"
)

const version = "1.5.0"

// Command-line options
var (
	all    bool
	repo   bool
	global bool
)

func init() {
	flag.BoolVar(&all, "a", false, "If set to true then process both global and repo settings")
	flag.BoolVar(&repo, "r", false, "If set to true then process only repo settings")
	flag.BoolVar(&global, "g", false, "If set to true then process only global settings")
}

func main() {

	// Recover from unhandled panics
	defer func() {
		if r := recover(); r != nil {
			reportError(r.(string))
		}
	}()

	flag.Parse()

	// Check all if not specified
	if (all || repo || global) == false {
		all = true
	}

	fix := false

	if flag.NArg() <= 1 {
		switch flag.Arg(0) {
		case "fix":
			fix = true
			fallthrough

		default:
			fallthrough
		case "check":
			run(fix)

		case "usage", "help":
			usage()

		case "version":
			fmt.Println("Gnit " + version)
		}
	} else {
		usage()
	}

}

func reportError(msg string) {
	fmt.Printf(colour.FgRedf+"\t%v\n", "ERROR", msg)
}

/*
Run all checks to report all possible problems.
If fix is true then also offer to fix known problems.
*/
func run(fix bool) {

	// To wait for all checks to finish before exiting.
	var wg sync.WaitGroup

	// To control access to console.
	// This is for the checks to run as far as possible in parallel only stopping if user input is needed.
	var cmux sync.Mutex

	// Global config
	if all || global {

		// Global Git config
		wg.Add(1)
		go func(fix bool, cmux *sync.Mutex) {
			defer wg.Done()
			defer func() {
				if r := recover(); r != nil {
					reportError(r.(string))
				}
			}()
			gitconfig.GlobalConfig(fix, cmux)

		}(fix, &cmux)

		// SSH config
		wg.Add(1)
		go func(fix bool, cmux *sync.Mutex) {
			defer wg.Done()
			defer func() {
				if r := recover(); r != nil {
					reportError(r.(string))
				}
			}()
			sshconfig.CheckSshConfig(fix, cmux)

		}(fix, &cmux)
	}

	if all || repo {

		// Git repo config
		wg.Add(1)
		go func(fix bool, cmux *sync.Mutex) {
			defer wg.Done()
			defer func() {
				if r := recover(); r != nil {
					reportError(r.(string))
				}
			}()
			gitconfig.RepoConfig(fix, cmux)

		}(fix, &cmux)

		// Git repo hooks
		wg.Add(1)
		go func(fix bool, cmux *sync.Mutex) {
			defer wg.Done()
			defer func() {
				if r := recover(); r != nil {
					reportError(r.(string))
				}
			}()
			gitconfig.RepoHooks(fix, cmux)

		}(fix, &cmux)
	}

	// Wait for checks to finish
	wg.Wait()

	fmt.Println()
}

// Print usage and exists
func usage() {
	fmt.Printf("Usage: %s [options] [usage|check|fix]\n", os.Args[0])
	fmt.Println()
	fmt.Println("Options:")
	flag.PrintDefaults()
	os.Exit(0)
}
