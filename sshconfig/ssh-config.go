// Gnit package for doing checks and fixes related to SSH.
package sshconfig

import (
	"bytes"
	"fmt"
	"github.com/sorennielsen/gnit/colour"
	"github.com/sorennielsen/gnit/util"
	"os"
	"os/user"
	"sync"
)

// CheckSshConfig checks, an optionally, fixes the SSH config related to Git.
func CheckSshConfig(fix bool, cmux *sync.Mutex) {

	// Output buffer until we are forced to gain access to console.
	var buffer bytes.Buffer
	defer func() {
		// Get access to console
		cmux.Lock()
		defer cmux.Unlock()
		fmt.Print(buffer.String())
	}()

	fmt.Fprintln(&buffer)
	fmt.Fprintf(&buffer, colour.Boldf+"\n", "Checking global SSH configuration")

	my, err := user.Current()
	if err != nil {
		panic("Unable to get current user from OS.")
	}

	configDir := my.HomeDir + "/.ssh"
	configFile := configDir + "/config"

	// Open config dir/file or create it if necessary.
	dirFound := util.FileExist(configDir)
	if dirFound {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tSSH configuration directory found.\n", "OK")
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\tSSH configuration directory not found [%s].\n", "FAIL", configDir)
		if fix {
			fmt.Fprintf(&buffer, colour.FgBluef+"\tCreating directory.\n", "FIX")
			err := os.Mkdir(configDir, 0700)
			if err != nil {
				panic("Could not create directory.")
			}
		}
	}

	fileFound := util.FileExist(configFile)
	if fileFound {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tSSH configuration file found.\n", "OK")
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\tSSH configuration file not found.\n", "FAIL")
		if fix {
			fmt.Fprintf(&buffer, colour.FgBluef+"\tCreating configuration file.\n", "FIX")
			file, err := os.OpenFile(configFile, os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0600)
			if err != nil {
				panic("Could not create configuration file.")
			}
			file.Close()
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tSSH configuration file created.\n", "SUCCESS")
		}
	}

	// Grep for "Host gerrit"
	found, err := util.Grep("Host gerrit", configFile)
	if err != nil {
		panic("Unable to read SSH configuration.")
	}
	if found {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tHost 'gerrit' configured.\n", "OK")
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\tHost 'gerrit' not configured.\n", "FAIL")

		if fix {
			fmt.Fprintf(&buffer, colour.FgBluef+"\tAdding 'Host gerrit'.\n", "FIX")
			file, err := os.OpenFile(configFile, os.O_APPEND|os.O_WRONLY, 0600)
			if err != nil {
				panic("Unable to write open configuration file for writing.")
			}
			defer file.Close()

			_, err = file.WriteString("Host gerrit\n")
			if err != nil {
				panic("Unable to write to configuration file.")
			}
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tHost written to configuration file.\n", "SUCCESS")

			fmt.Fprintf(&buffer, colour.FgBluef+"\tAdding 'Port 29418'.\n", "FIX")
			_, err = file.WriteString("\tPort 29418\n")
			if err != nil {
				panic("Unable to write to configuration file.")
			}
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tPort written to configuration file.\n", "SUCCESS")
		}
	}
}
