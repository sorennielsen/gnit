package gitconfig

import (
	"bytes"
	"fmt"
	"github.com/sorennielsen/gnit/colour"
	"github.com/sorennielsen/gnit/util"
	"io"
	"net/http"
	"os"
	"sync"
)

const (
	gerritCommitHookPath = /* GIT_DIR/ */ "/hooks/commit-msg"
	gerritCommitHookURL  = "http://gerrit.googlecode.com/git/gerrit-server/src/main/resources/com/google/gerrit/server/tools/root/hooks/commit-msg"
	nocommitHookPath     = /* GIT_DIR/ */ "/hooks/pre-commit"
	nocommitHookURL      = "https://raw.github.com/sorennielsen/git-hooks/master/pre-commit-nocommit.sh"
)

func RepoHooks(fix bool, cmux *sync.Mutex) {

	// Output buffer until we are forced to gain access to console.
	var buffer bytes.Buffer

	defer func() {
		// Get access to console
		cmux.Lock()
		defer cmux.Unlock()
		fmt.Print(buffer.String())
	}()

	fmt.Fprintln(&buffer)
	fmt.Fprintf(&buffer, colour.Boldf+"\n", "Checking Git hooks")

	// Check if actially inside Git repository
	gitDir, err := GitDir()
	if err != nil {
		panic("Not a git repository.")
	}

	out := make(chan []byte)

	go checkHook(out, fix, "Gerrit", gitDir+gerritCommitHookPath, gerritCommitHookURL)
	go checkHook(out, fix, "NOCOMMIT", gitDir+nocommitHookPath, nocommitHookURL)

	// Wait or synch before return
	buffer.Write(<-out)
	buffer.Write(<-out)
}

func checkHook(out chan []byte, fix bool, name string, hookPath string, hookURL string) {
	var buffer bytes.Buffer
	defer func() {
		out <- buffer.Bytes()
	}()

	found := util.FileExist(hookPath)
	if found {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tFound %s hook\n", "OK", name)
		// TODO: Verify that this is indeed the hook. Either by grepping for {name} or
		// making diff to content of hookURL. This would also work as a "check for updates" function.
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\t%s hook not installed [%s]\n", "FAIL", name, hookPath)
		if fix {
			fmt.Fprintf(&buffer, colour.FgBluef+"\tInstalling %s hook.\n", "FIX", name)
			err := download(hookPath, hookURL)
			if err != nil {
				panic(fmt.Sprintf("Unable to download %s hook.", name))
			}
			fmt.Fprintf(&buffer, colour.FgGreenf+"\t%s hook downloaded and installed.\n", "SUCCESS", name)
		}
	}
}

// Download GETs the content from source URL and writes it to the given destination.
// Destination is never overwritten and a panic is started if file already exist or
// cannot be opened with exclusive write access.
// TODO: Make this a general utility.
func download(destFilename string, srcUrl string) error {

	file, err := os.OpenFile(destFilename, os.O_EXCL|os.O_WRONLY|os.O_CREATE, 0750)
	if err != nil {
		panic("Unable to create file with exclusive write access.")
	}
	defer file.Close()

	resp, err := http.Get(srcUrl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if _, err := io.Copy(file, resp.Body); err != nil {
		panic("Unable to write content of downloaded hook.")
	}

	// All good
	return nil
}
