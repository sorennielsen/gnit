package gitconfig

import (
	"bufio"
	"bytes"
	"fmt"
	conf "github.com/hgfischer/goconf"
	"github.com/sorennielsen/gnit/colour"
	"os"
	"os/user"
	"sync"
	"time"
)

// Check and optionally fix global Git configuration
func GlobalConfig(fix bool, cmux *sync.Mutex) {

	// Output buffer until we are forced to gain access to console.
	var buffer bytes.Buffer

	anythingToWrite := false

	my, err := user.Current()
	if err != nil {
		panic("Unable to get current user from OS.")
	}

	configFile := my.HomeDir + "/.gitconfig"

	fmt.Fprintln(&buffer)
	fmt.Fprintf(&buffer, colour.Boldf+"\n", "Checking global Git configuration")
	c, err := conf.ReadConfigFile(configFile)
	if err != nil {
		fmt.Fprintf(&buffer, colour.FgRedf+"\t%v\n", "FAIL", err.Error())
		if fix {
			c = conf.NewConfigFile()
			fmt.Fprintf(&buffer, colour.FgBluef+"\tCreating new configuration.\n", "FIX")
			anythingToWrite = true
		}
	}

	if c != nil {

		// Check for user.name
		if c.HasOption("user", "name") {
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tuser.name found.\n", "OK")
		} else {
			fmt.Fprintf(&buffer, colour.FgRedf+"\tuser.name was not found.\n", "FAIL")
			if fix {
				fmt.Fprintf(&buffer, colour.FgYellowf+"\tUnable to fix this for you.\n", "WARNING")
			}
			fmt.Fprintln(&buffer, "\tUse 'git config --global user.name [your name]' to fix it.")
		}

		// Check for user.email
		if c.HasOption("user", "email") {
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tuser.email found.\n", "OK")
		} else {
			fmt.Fprintf(&buffer, colour.FgRedf+"\tuser.email was not found\n", "FAIL")
			if fix {
				fmt.Fprintf(&buffer, colour.FgYellowf+"\tUnable to fix this for you.\n", "WARNING")
			}
			fmt.Fprintln(&buffer, "\tUse 'git config --global user.email [your email]' to fix it.")
		}

		// Check for push.default
		name, _ := c.GetString("push", "default")
		if name == "simple" {
			fmt.Fprintf(&buffer, colour.FgGreenf+"\tpush.default = simple\n", "OK")
		} else {
			fmt.Fprintf(&buffer, colour.FgRedf+"\tpush.default is '%s' but should be '%s'\n", "FAIL", name, "simple")
			if fix {
				fmt.Fprintf(&buffer, colour.FgBluef+"\tSetting push.default to 'simple'", "FIX")
				c.AddOption("push", "default", "simple")
				anythingToWrite = true
			}
		}

		// Get access to console
		cmux.Lock()
		defer cmux.Unlock()
		fmt.Print(buffer.String())
		buffer.Reset()

		if fix {
			if anythingToWrite {
				fmt.Println()
				fmt.Printf("Configuration to be written to '%s':\n", configFile)
				c.Write(os.Stdout, fmt.Sprintf("Written/updated by gnit, %v", time.Now()))

				fmt.Println()

				fmt.Print("Do you want to write these changes? [yN]: ")

				in := bufio.NewReader(os.Stdin)

				r, _, _ := in.ReadRune()
				if r == 'y' {
					// Notice that the file permisions "0640" is not implemented by the conf package
					err := c.WriteConfigFile(configFile, 0640, fmt.Sprintf("Written/updated by gnit, %v", time.Now()))
					if err != nil {
						panic(err.Error())
					}
					fmt.Printf(colour.FgGreenf+"\tGit configuration written\n", "SUCCESS")
				} else {
					fmt.Println("Aborting...")
				}
			}
		}
	}
}
