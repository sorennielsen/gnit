package gitconfig

import (
	"bufio"
	"bytes"
	"fmt"
	conf "github.com/hgfischer/goconf"
	"github.com/sorennielsen/gnit/colour"
	"os"
	"sync"
	"time"
)

func RepoConfig(fix bool, cmux *sync.Mutex) {
	// Output buffer until we are forced to gain access to console.
	var buffer bytes.Buffer
	defer func() {
		// Get access to console
		cmux.Lock()
		defer cmux.Unlock()
		fmt.Print(buffer.String())
	}()

	anythingToWrite := false

	fmt.Fprintln(&buffer)
	fmt.Fprintf(&buffer, colour.Boldf+"\n", "Checking Git repo setup")

	// Check if actually inside Git repository
	gitDir, err := GitDir()
	if err != nil {
		panic("Not a git repository.")
	}

	// Load config file
	configFile := gitDir + "/config"
	c, err := conf.ReadConfigFile(configFile)
	if err != nil {
		fmt.Fprintf(&buffer, colour.FgRedf+"\t%s\n", "FAIL", err.Error())
		if fix {
			fmt.Fprintf(&buffer, colour.FgYellowf+"\tUnable to fix this for you.\n", "WARNING")
		}
		return
	}

	// Check remote and "refs/for/develop"
	hasReview := c.HasSection("remote \"review\"")
	if !hasReview {
		if fix {
			fmt.Fprintf(&buffer, colour.FgRedf+"\tRemote 'review' not found.\n", "FAIL")
			c.AddSection("remote \"review\"")
			fmt.Fprintln(&buffer, "\tAdding remote 'review'.")
			anythingToWrite = true
		}
	}

	// Verify url
	if c.HasOption("remote \"review\"", "url") {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tRemote review.url found.\n", "OK")
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\tRemote review.url not correct.\n", "FAIL")
		if fix {
			if c.HasOption("remote \"origin\"", "url") {
				// BUG: If origin is not gerrit:* then this check will continue to fail!
				originUrl, _ := c.GetString("remote \"origin\"", "url")
				fmt.Fprintf(&buffer, colour.FgBluef+"\tSetting remote review.url = '%s'\n", "FIX", originUrl)
				c.AddOption("remote \"review\"", "url", originUrl)
				anythingToWrite = true
			} else {
				fmt.Fprintf(&buffer, colour.FgYellowf+"\tUnable to fix this for you.\n", "WARNING")
			}
		}
	}

	// Verify push
	push, _ := c.GetString("remote \"review\"", "push")
	if push == "HEAD:refs/for/develop" {
		fmt.Fprintf(&buffer, colour.FgGreenf+"\tRemote review.push = HEAD:refs/for/develop\n", "OK")
	} else {
		fmt.Fprintf(&buffer, colour.FgRedf+"\tRemote review.push is '%s' but should be '%s'\n", "FAIL", push, "HEAD:refs/for/develop")
		if fix {
			fmt.Fprintf(&buffer, colour.FgBluef+"\tSetting remote review.push to 'HEAD:refs/for/develop'\n", "FIX")
			c.AddOption("remote \"review\"", "push", "HEAD:refs/for/develop")
			anythingToWrite = true
		}
	}

	// Get access to console
	cmux.Lock()
	defer cmux.Unlock()
	fmt.Print(buffer.String())
	buffer.Reset()

	if fix {
		if anythingToWrite {
			fmt.Println()
			fmt.Printf("Configuration to be written to '%s':\n", configFile)
			c.Write(os.Stdout, fmt.Sprintf("Written/updated by gnit, %v", time.Now()))

			fmt.Println()

			fmt.Print("Do you want to write these changes? [yN]: ")

			in := bufio.NewReader(os.Stdin)

			r, _, _ := in.ReadRune()
			if r == 'y' {
				// Notice that the file permisions "0640" is not implemented by the conf package
				err := c.WriteConfigFile(configFile, 0640, fmt.Sprintf("Written/updated by gnit, %v", time.Now()))
				if err != nil {
					panic(err.Error())
				}
				fmt.Printf(colour.FgGreenf+"\tGit configuration written\n", "SUCCESS")
			} else {
				fmt.Println("Aborting...")
			}
		}
	}
}
