package gitconfig

import (
	_ "errors"
	_ "fmt"
	"os/exec"
	"strings"
)

func GitDir() (gitDir string, err error) {

	// Check if actially inside Git repository
	cmd := exec.Command("git", "rev-parse", "--git-dir")
	out, err := cmd.Output()
	if err != nil {
		return
	}
	gitDir = strings.TrimSpace(string(out))
	return
}
